from dotenv import load_dotenv
from flask import url_for
from flask_migrate import Migrate, MigrateCommand
from flask_script import Manager

from main import create_app
from model.db import db

load_dotenv()
app = create_app()
manager = Manager(app)
migrate = Migrate(app, db, compare_type=True)
manager.add_command('db', MigrateCommand)


@manager.command
def list_routes():
    import urllib.parse
    output = []
    for rule in app.url_map.iter_rules():

        options = {}
        for arg in rule.arguments:
            options[arg] = "[{0}]".format(arg)

        methods = ','.join(rule.methods)
        url = url_for(rule.endpoint, **options)
        line = urllib.parse.unquote("{:50s} {:20s} {}".format(rule.endpoint, methods, url))
        output.append(line)

    for line in sorted(output):
        print(line)


@manager.command
def create_admin(email, password, name):
    try:
        from model import User
        from config import UserStatus
        from config import UserRole
        from config import DEFAULT_PASSWORD_ADMIN
        user_admin = User(
            status=UserStatus.ACTIVE,
            role=UserRole.ADMIN,
            title='Mr',
            email=email,
            name=name or 'admin',
            family_name='admin',
            office_number='000000000',
            phone_number='999999999',
        )
        user_admin.set_password(password or DEFAULT_PASSWORD_ADMIN)
        user_admin.save(session=db.session)
        print(f'created admin: {user_admin.to_json()}')
    except Exception as e:
        print(f'error: {str(e)}')


if __name__ == '__main__':
    manager.run()
