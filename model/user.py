from datetime import datetime

from sqlalchemy import Column, DateTime, ForeignKey, String, Boolean
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy.orm import relationship

from model.db import Base
from utils.dict import DotDict
from utils.hash_util import hash_password, check_password


class User(Base):
    __tablename__ = 'user'
    __json_hidden__ = [
        'password_hash'
    ]

    ROLE = DotDict({
        'CUSTOMER': 'customer',
        'ADMIN': 'admin'
    })

    STATUS = DotDict({
        'INACTIVE': 'inactive',
        'ACTIVE': 'active',
    })

    # Columns
    email = Column(String, nullable=False, unique=True)
    password_hash = Column(String)

    role = Column(String, nullable=False, default=ROLE.CUSTOMER)
    status = Column(String, nullable=False, default=STATUS.ACTIVE)
    title = Column(String, nullable=False)
    name = Column(String, nullable=False)
    family_name = Column(String, nullable=False)

    phone_number = Column(String, nullable=False)
    country_code_phone_number = Column(String)
    country_phone_number = Column(String)

    office_number = Column(String, nullable=False)
    country_code_office_number = Column(String)
    country_office_number = Column(String)

    company_id = Column(UUID(as_uuid=True), ForeignKey('company.id', ondelete='CASCADE'))
    company = relationship('Company', lazy='selectin')
    department = Column(String)
    position = Column(String)

    last_request_at = Column(DateTime, default=datetime.utcnow)

    verified = Column(Boolean, nullable=False, default=False)

    @hybrid_property
    def full_phone_number(self):
        return f'{self.country_code_phone_number or ""}{self.phone_number}'

    @hybrid_property
    def full_office_number(self):
        return f'{self.country_code_office_number or ""}{self.office_number}'

    def set_password(self, password):
        self.password_hash = hash_password(password)

    def check_password(self, password):
        return check_password(self.password_hash, password)

    def request(self, is_commit=True):
        self.last_request_at = datetime.utcnow()
        if is_commit:
            self.save()
