import uuid

from flask_sqlalchemy import Model, SQLAlchemy, BaseQuery
from sqlalchemy import MetaData, func, desc, asc, ARRAY, JSON
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy.inspection import inspect

from utils.dict import generate_json
from utils.exceptions import NotFound


class JsonSerializer(object):
    """
    Reference: https://github.com/mattupstate/overholt/blob/master/overholt/helpers.py#L45

    A mixin that can be used to mark a SQLAlchemy model class which
    implements a :func:`to_json` method. The :func:`to_json` method is used
    in conjuction with the custom :class:`JSONEncoder` class. By default this
    mixin will assume all properties of the SQLAlchemy model are to be visible
    in the JSON output. Extend this class to customize which properties are
    public, hidden or modified before being being passed to the JSON serializer.
    """

    __json_public__ = None
    __json_hidden__ = None
    __json_modifiers__ = None

    def get_field_names(self):
        for p in self.__mapper__.iterate_properties:
            yield p.key
        for p in inspect(self.__class__).all_orm_descriptors:
            if type(p) == hybrid_property:
                yield p.__name__

    def to_json(self):
        field_names = self.get_field_names()
        public = self.__json_public__ or field_names
        modifiers = self.__json_modifiers__ or dict()
        rv = dict()
        for key in public:
            rv[key] = getattr(self, key)
        for key, modifier in modifiers.items():
            value = getattr(self, key)
            rv[key] = modifier(value, self)
        return generate_json(rv)


class ModelGeneralTasks(object):
    __update_field__ = None

    def save(self, session=None, is_commit=True):
        try:
            session = session or inspect(self).session
            session.add(self)
            if is_commit:
                session.commit()
        except Exception as e:
            session.rollback()
            raise e
        return self

    def delete(self, session=None, is_commit=True):
        try:
            session = session or inspect(self).session
            session.delete(self)
            if is_commit:
                session.commit()
        except Exception as error:
            session.rollback()
            raise error

    def update(self, data, session=None, is_commit=True):
        fields_can_update = self.__update_field__ or None
        for k, v in data.items():
            if fields_can_update is None or k in fields_can_update:
                setattr(self, k, v)
        try:
            session = session or inspect(self).session
            if session is not None and is_commit:
                session.commit()
        except Exception as error:
            session.rollback()
            raise error
        return self

    @classmethod
    def find_type(cls, colname):
        if hasattr(cls, '__table__') and colname in cls.__table__.c:
            return cls.__table__.c[colname].type
        raise NameError(colname)


class PowerPaintQuery(BaseQuery):
    def dynamic_filter(self, model, rfilter: dict):
        query = self
        for k, v in rfilter.items():
            coltype = model.find_type(k)
            if isinstance(coltype, ARRAY):
                query = query.filter(model.__table__.columns[k].cast(
                    ARRAY(coltype.item_type.__class__)
                ).overlap(v if isinstance(v, list) else [v]))
            elif isinstance(coltype, JSON):
                pass
            else:
                if isinstance(v, dict):
                    if 'min' in v:
                        query = query.filter(model.__table__.columns[k] >= v['min'])
                    if 'max' in v:
                        query = query.filter(model.__table__.columns[k] <= v['max'])
                if isinstance(v, list):
                    query = query.filter(model.__table__.columns[k].in_(v))
                else:
                    query = query.filter(model.__table__.columns[k] == v)
        return query

    def search(self, model, rfilter: dict):
        query = self
        for k, v in rfilter.items():
            query = query.filter(
                model.__table__.columns[k].ilike(f'%{v}%'))
        return query

    def sort(self, sort_by, sort_type):
        query = self
        if isinstance(sort_type, str):
            if sort_type.lower() == 'desc':
                query = query.order_by(desc(sort_by))
            else:
                query = query.order_by(asc(sort_by))
        else:
            query = query.order_by(sort_type(sort_by))
        return query

    def get_with_paginate(self, offset, limit):
        items = self.offset(offset).limit(limit).all()
        count = self.count()
        return items, count

    def existed(self):
        return self.session.query(self.exists()).scalar()

    def get_or_404(self, rid, custom_message=None):
        data = self.get(rid)
        if data is None:
            if custom_message is not None:
                raise NotFound('{custom_message}: {id}'.format(
                    custom_message=custom_message, id=str(rid)))
            raise NotFound('Object does not exist: {id}'.format(id=str(rid)))
        return data

    def first_or_404(self, custom_message=None):
        data = self.first()
        if data is None:
            if custom_message is not None:
                raise NotFound('{custom_message}'.format(custom_message=custom_message))
            raise NotFound('Object does not exist')
        return data

    def filter_or_404(self, model, rfilter, custom_message=None):
        message_error = 'Object does not exist'
        if custom_message is not None and isinstance(custom_message, str):
            message_error = custom_message
        rv = self.find_by_filter(model, rfilter)
        if len(rv) == 0:
            raise NotFound(message_error)
        return rv[0]

    def max(self, column, rfilter):
        # type: (object, object) -> int
        return self.session.query(func.max(column)).filter(rfilter)

    def min(self, column, rfilter):
        # type: (object, object) -> int
        return self.session.query(func.min(column)).filter(rfilter)


class PowerPaintModel(Model, JsonSerializer, ModelGeneralTasks):
    pass


convention = {
    "ix": 'ix_%(column_0_label)s',
    "uq": "uq_%(table_name)s_%(column_0_name)s",
    "ck": "ck_%(table_name)s",
    "fk": "fk_%(table_name)s_%(column_0_name)s_%(referred_table_name)s",
    "pk": "pk_%(table_name)s"
}
metadata = MetaData(naming_convention=convention)

db = SQLAlchemy(metadata=metadata, model_class=PowerPaintModel, query_class=PowerPaintQuery)


class Base(db.Model):
    __abstract__ = True

    id = db.Column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid4)
    created_at = db.Column(db.DateTime, default=db.func.current_timestamp())
    updated_at = db.Column(db.DateTime, default=db.func.current_timestamp(), onupdate=db.func.current_timestamp())
