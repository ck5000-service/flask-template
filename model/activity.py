import uuid
from datetime import datetime

from sqlalchemy import Column, DateTime, String, ForeignKey, JSON
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.orm import relationship

from model.db import Base


class Activity(Base):
    __tablename__ = 'activity'

    # Columns
    id = Column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid4)
    user_id = Column(UUID(as_uuid=True), ForeignKey('user.id'))
    user = relationship('User', lazy='selectin')
    action = Column(String, nullable=False)
    detail = Column(JSON)
    created_at = Column(DateTime, default=datetime.utcnow)
