from sqlalchemy import Column, DateTime, String, ForeignKey
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.orm import relationship

from model.db import Base
from utils.dict import DotDict


class OneTimePassword(Base):
    __tablename__ = 'one_time_password'

    TYPE = DotDict({
        'RESET_PASSWORD': 'reset password',
        'ACTIVATE_ACCOUNT': 'activate account'
    })

    # Columns
    user_id = Column(UUID(as_uuid=True), ForeignKey('user.id'))
    user = relationship('User')
    type = Column(String)
    code = Column(String)
    code_expiry_at = Column(DateTime)
