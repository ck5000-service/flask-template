from sqlalchemy import Column, DateTime, String

from model.db import Base
from utils.array import find
from utils.dict import DotDict


class Company(Base):
    __tablename__ = 'company'
    __update_field__ = [
        'type',
        'name',
        'website',
        'registration_number',
        'address1',
        'address2',
        'state',
        'zip_code',
        'city',
        'country'
    ]

    STATUS = DotDict({
        'INACTIVE': 'inactive',
        'ACTIVE': 'active',
    })

    # Columns
    status = Column(String, nullable=False, default=STATUS.ACTIVE)

    type = Column(String, nullable=False)
    name = Column(String, nullable=False)
    website = Column(String, nullable=True)
    registration_number = Column(String, nullable=False)
    address1 = Column(String, nullable=False)
    address2 = Column(String)
    state = Column(String)
    zip_code = Column(String, nullable=False)
    city = Column(String, nullable=False)
    country = Column(String, nullable=False)

    deleted_at = Column(DateTime)

    def get_country_code(self):
        country_code = find(lambda x: x.get('country', None) == self.country).get('phone number', None)
        return country_code

    def inactive(self):
        self.status = self.STATUS.INACTIVE

    def active(self):
        self.status = self.STATUS.ACTIVE
