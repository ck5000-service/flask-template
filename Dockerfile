FROM python:3.7.6-slim-stretch

ENV PYTHONUNBUFFERED 1
ENV DOCKER 1

# Setup for psycopg2-binary and make
RUN apt-get update && apt-get install -y apt-utils git vim make netcat wget

# Install wkhtmltopdf 0.12.3 (with patched qt)
RUN apt-get install -y libfontenc1 libjpeg62-turbo libxfont1 x11-common xfonts-75dpi xfonts-base xfonts-encodings xfonts-utils fontconfig libxrender1
RUN wget https://github.com/wkhtmltopdf/wkhtmltopdf/releases/download/0.12.5/wkhtmltox_0.12.5-1.stretch_amd64.deb
RUN dpkg -i wkhtmltox_0.12.5-1.stretch_amd64.deb

WORKDIR /app

# Application Environment
ADD requirements.txt ./
RUN pip install --upgrade pip
RUN pip install -r requirements.txt

ADD . .
