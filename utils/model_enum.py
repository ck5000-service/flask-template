from enum import Enum


class ISenseEnum(Enum):
    @classmethod
    def list(cls):
        return [t.value for t in cls]
