import re
import uuid
from datetime import datetime
from functools import wraps
from itertools import chain

from config import COUNTRIES
from utils.exceptions import UserInputInvalid, ApplicationError

continent_values = list(chain(*list(COUNTRIES[group] for group in COUNTRIES.keys())))
valid_countries_names = list(item['country'] for item in continent_values)
valid_countries_codes = list(item['code'] for item in continent_values)
valid_countries_number_codes = list(item['number_code'] for item in continent_values)

DATE_FORMAT = "%Y-%m-%d"
DATETIME_FORMAT = "%Y-%m-%dT%H:%M:%S%z"

EMAIL_PATTERN = r"(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-\.]+$)"
PASSWORD_PATTERN = r"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)" \
                   r"(?=.*[!\"#$%&'()*+,-./:;<=>\?@\[\\\]^_`\{\|\}\~])" \
                   r"[A-Za-z\d!\"#$%&'()*+,-./:;<=>\?@\[\\\]^_`\{\|\}\~]{8,}$"
URL_PATTERN = r"^https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)?"
FILENAME_PATTERN = r"(^[a-zA-Z0-9._ -]+$)"
NUMBER_PATTERN = r"(^[0-9]+$)"


def email_validator(email: str):
    pattern = re.compile(EMAIL_PATTERN)
    if pattern.match(email):
        return True
    return False


def number_validator(number: str):
    pattern = re.compile(NUMBER_PATTERN)
    if pattern.match(number):
        return True
    return False


def password_validator(password: str):
    pattern = re.compile(PASSWORD_PATTERN)
    if pattern.match(password):
        return True
    return False


def url_validator(url: str):
    pattern = re.compile(URL_PATTERN)
    if pattern.match(url):
        return True
    return False


def filename_validator(filename: str):
    if filename is None:
        return False
    pattern = re.compile(FILENAME_PATTERN)
    if pattern.match(filename):
        return True
    return False


def uuid_schema(value):
    try:
        uuid.UUID(value)
        return value
    except ValueError:
        raise UserInputInvalid(f"'{value}' is wrong uuid format'")


def date_schema(value):
    try:
        return datetime.strptime(value, DATE_FORMAT)
    except TypeError:
        raise UserInputInvalid(f"'{value}' is wrong date format '{DATE_FORMAT}'")


def datetime_schema(value):
    try:
        return datetime.strptime(value, DATETIME_FORMAT)
    except TypeError:
        raise UserInputInvalid(f"'{value}' is wrong datetime format '{DATETIME_FORMAT}'")


def country_schema(value):
    if value not in valid_countries_names:
        raise UserInputInvalid(f"Invalid Geography name: {value}")
    return value


def country_code_schema(value):
    if value not in valid_countries_codes:
        raise UserInputInvalid(f"Invalid Geography code: {value}")
    return value


def country_number_code_schema(value):
    if value not in valid_countries_number_codes:
        raise UserInputInvalid(f"Invalid Geography number code: {value}")
    return value


def email_schema(value):
    if not email_validator(value):
        raise UserInputInvalid(f"{value} is wrong email format")
    return value


def number_schema(value):
    if not number_validator(value):
        raise UserInputInvalid(f'{value} must be a number')
    return value


def float_or_int_schema(value):
    if not isinstance(value, (int, float)):
        raise UserInputInvalid(f'Value must be a number: {value}')
    return round(value, 2)


def password_schema(value):
    if not password_validator(value):
        raise UserInputInvalid(f"password is wrong format")
    return value


def url_schema(value):
    if not url_validator(value):
        raise UserInputInvalid(f"{value} is wrong url format")
    return value


def filename_schema(value):
    if not filename_validator(value):
        raise UserInputInvalid(f"{value} is wrong filename format")
    return value


def string_schema(value):
    if not isinstance(value, str):
        raise UserInputInvalid(f'{value} must be a string')
    if not bool(value.strip()):
        raise UserInputInvalid(f"String not empty")
    return value


def string_or_none_schema(value):
    if not isinstance(value, str):
        raise UserInputInvalid(f'{value} must be a string')
    if not bool(value.strip()):
        return None
    return value


def validated(schema_validator):
    """
    Using to validate schema of request.data. Raise Voluptuous.Invalid if failed.
    """

    def internal(func):
        @wraps(func)
        def decorated(*args, **kwargs):
            from flask import request
            if not request.is_json:
                raise ApplicationError("Bad Request", detail="Expect json type body")
            data = schema_validator(request.get_json())
            request.data = data
            return func(*args, **kwargs)

        return decorated

    return internal
