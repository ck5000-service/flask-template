def find(f, seq):
    """Return first item in sequence where f(item) == True."""
    for item in seq:
        if f(item):
            return item


def issubarray(obj, array: list):
    """Return True if obj is a item or subarray of array"""
    if isinstance(obj, (str, int, float)):
        if obj in array:
            return True
    if isinstance(obj, list):
        if all(item in array for item in obj):
            return True
    return False


def get_average(lst):
    return sum(lst) / len(lst)


def get_iterable(value):
    if isinstance(value, list):
        return value
    return [value]


def unnest(lst: list):
    rv = list()
    for i in lst:
        if isinstance(i, list):
            rv.extend(unnest(i))
        else:
            rv.append(i)
    return rv
