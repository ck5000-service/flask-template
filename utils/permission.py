from datetime import datetime
from functools import wraps

from flask_jwt_extended import get_jwt_identity

from config import TIME_EXPIRY_TOKEN_NOT_REQUEST
from utils.dict import DotDict
from utils.exceptions import PermissionDenied, BadRequest


def authorized(roles: list):
    def real_jwt_required(fn):
        @wraps(fn)
        def internal(*args, **kwargs):
            user = get_requester()
            if user is None:
                raise BadRequest('User not found')
            check_user_permission(user=user, roles=roles)
            now = datetime.utcnow()
            if user.last_request_at and user.last_request_at < (now - TIME_EXPIRY_TOKEN_NOT_REQUEST):
                raise PermissionDenied('Token has expired')
            user.request()
            return fn(*args, **kwargs)

        return internal

    return real_jwt_required


def check_user_permission(user, roles: list):
    if user.status not in ['active']:
        raise PermissionDenied('User not active')
    if user.role not in roles:
        raise PermissionDenied(f'Role {user.role} is not allowed')


def get_requester():
    from model import User
    user_id = get_jwt_identity()
    user = User.query.get(user_id)
    return user


ACCOUNT_PERMISSION = DotDict({
    'ADMIN': 'admin',
    'CUSTOMER': 'customer'
})
