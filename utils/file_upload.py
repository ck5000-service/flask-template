import os
from datetime import datetime

from config import ALLOWED_EXCEL_EXTENSIONS


def allowed_excel_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXCEL_EXTENSIONS


def create_folder_upload(directory):
    if not os.path.exists(directory):
        os.makedirs(directory)


def get_filename(filename, prefix=None):
    extension = os.path.splitext(filename)[1]
    timestamp = datetime.utcnow().timestamp()
    filename = f'{timestamp}{extension}'
    if prefix:
        filename = prefix + filename
    return filename
