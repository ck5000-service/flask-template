import datetime
import enum
import json
import uuid
from itertools import groupby

from sqlalchemy.engine.result import RowProxy


class DotDict(dict):
    """dot.notation access to dictionary attributes"""
    __getattr__ = dict.get
    __setattr__ = dict.__setitem__
    __delattr__ = dict.__delitem__


def generate_json(obj, deep=True, hidden=None):
    if hidden is None:
        hidden = []
    if hasattr(obj, "to_json") and deep:
        return obj.to_json(deep=deep, hidden=hidden)
    elif isinstance(obj, dict):
        for item in obj:
            item_hidden = [i[len(f'{item}.'):] for i in hidden if i.startswith(f'{item}.')]
            obj[item] = generate_json(obj[item], deep=deep, hidden=item_hidden)
        return obj
    elif isinstance(obj, list):
        return [generate_json(element, deep=deep) for element in obj]
    elif isinstance(obj, datetime.datetime):
        return obj.isoformat()
    elif isinstance(obj, uuid.UUID):
        return str(obj)
    elif isinstance(obj, (int, float, bool)):
        return obj
    elif isinstance(obj, enum.Enum) and hasattr(obj, "name"):
        return obj.name
    elif obj is None:
        return None
    else:
        return str(obj)


def nested_get(input_data, nested_keys, default_value=None):
    internal_value = input_data
    for k in nested_keys:
        if isinstance(internal_value, dict):
            internal_value = internal_value.get(k, default_value)
        elif isinstance(internal_value, list) and len(internal_value) > int(k):
            internal_value = internal_value[int(k)]
        elif isinstance(internal_value, RowProxy):
            internal_value = internal_value[int(k)]
        else:
            try:
                internal_value = getattr(internal_value, k)
            except AttributeError:
                return default_value
        if internal_value is None:
            return default_value
    return internal_value


def destructure_dict(dictionary, *args):
    return [nested_get(dictionary, [arg], None) for arg in args]


def self_serialize_objects_to_dicts(objects_array, serializer_function_name, serializer_params):
    dicts = []
    for obj in objects_array:
        serializer_function = getattr(obj, serializer_function_name)
        if serializer_function is not None:
            dicts.append(serializer_function(**serializer_params))
        else:
            dicts.append(None)

    return dicts


def group_array_of_json(array: list, *keys, end_key):
    keys = list(keys)
    json_grouped = {}
    if len(keys) == 0:
        for new_key, group in groupby(array, key=lambda json: json[end_key]):
            json_grouped[new_key] = list(group)
        return json_grouped
    key = keys.pop(0)
    for new_key, group in groupby(array, key=lambda json: json[key]):
        json_grouped[new_key] = _get_only(list(group), end_key) if len(keys) == 0 \
            else group_array_of_json(list(group), *keys, end_key=end_key)
    return json_grouped


def _get_only(array: list, key):
    return list(map(lambda item: item[key], array))


def get_data_by_keys(json: dict, keys: list):
    rv = {}
    for key in keys:
        if not isinstance(key, str):
            continue
        rv[key] = json.get(key, None)
    return rv


def json_to_doted(json, tgt=None, path=None):
    tgt = tgt or {}
    path = path or []
    for index, value in (enumerate(json) if isinstance(json, list) else json.items()):
        internal_path = path.copy()
        internal_path.append(index)
        if isinstance(value, (list, dict)) and value:
            tgt = json_to_doted(value, tgt, internal_path)
        else:
            tgt['.'.join(map(str, internal_path))] = value
    return tgt


def doted_to_json(doted, json=None):
    json = json or {}
    for index, value in doted.items():
        path = [(int(value) if is_int(value) else value) for value in index.split('.')]
        nested_set(json, path, value)
    return json


def nested_set(json, keys: list, value=None):
    key = keys.pop(0)
    if keys:
        if isinstance(json, dict):
            json.setdefault(key, ([] if isinstance(keys[0], int) else {}))
        if isinstance(json, list) and len(json) <= key:
            json.append(([] if isinstance(keys[0], int) else {}))
        nested_set(json[key], keys, value)
    else:
        if isinstance(json, dict):
            json[key] = value
        if isinstance(json, list):
            json.insert(key, value)


def is_int(value):
    try:
        int(value)
        return True
    except:
        return False


def is_json(value):
    try:
        json.loads(value)
        return True
    except:
        return False


def convert_to_json(value):
    try:
        json_value = json.loads(value)
        return json_value
    except:
        return None
