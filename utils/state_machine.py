from typing import List


class Event:
    def __init__(self, allowed_states: List[str], to_state):
        self.allowed_states = allowed_states
        self.to_state = to_state

    def is_allowed_state(self, state: str):
        return bool(state in self.allowed_states)

    def get_state(self):
        return self.to_state
