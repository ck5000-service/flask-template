from config import APP_CONTACT, ENDPOINT, FRONTEND_ENDPOINT
from services.mail import send_email as send_email_service

CHARSET = "UTF-8"
link_contact_us = f'https://mail.google.com/mail/?view=cm&fs=1&to={APP_CONTACT}'
template_path = 'templates/mail-template'

original_path = f'{ENDPOINT}/conf/static/original.png'


def _send_mail(to, subject, template, data):
    path_file = f'{template_path}/{template}.html'
    with open(path_file) as f:
        html = f.read()
    message = html.format(
        link_contact_us=link_contact_us,
        original=original_path,
        **data
    )
    send_email_service.delay(to, subject, message)


def send_mail_reset_password(user, code):
    template = 'resetpassword'
    reset_link = f'{FRONTEND_ENDPOINT}/reset-password?token={code}'
    data = {
        'user': user,
        'reset_link': reset_link
    }
    subject = 'iSense reset password'
    _send_mail(user.email, subject, template, data)


def send_mail_welcome(user):
    template = 'welcome'
    data = {'user': user}
    subject = 'Welcome to iSense'
    _send_mail(user.email, subject, template, data)


def send_mail_deny(user):
    template = 'deny'
    data = {'user': user}
    subject = 'iSense denied user'
    _send_mail(user.email, subject, template, data)
