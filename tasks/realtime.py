import os
from urllib.parse import urljoin

import requests

from celery_app import celery


@celery.task(name='tasks.readtime.send_to_service')
def send_to_service(data):
    try:
        url = urljoin(os.getenv("READTIME_SERVICE_ENDPOINT"), "webhook/server")
        result = requests.post(
            url=url,
            headers={"Authentication": os.getenv("READTIME_SERVICE_AUTH_TOKEN")},
            json=data
        )
        print(result.json())
    except Exception as e:
        print(f'Request to readtime service error: {str(e)}')
