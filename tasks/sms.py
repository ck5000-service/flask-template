from services.sms import send_sms


def send_otp(to_phone_number, otp):
    message = f"<iSense>: Your OTP is {str(otp)} for your iSense log in"
    send_sms.delay(to_phone_number, message)
