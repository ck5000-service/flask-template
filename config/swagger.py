SWAGGER_CONFIG = {
    "swagger": "2.0",
    "info": {
        "title": "API",
        "description": "API",
        "contact": {},
        "termsOfService": "http://rockship.co/#",
        "version": "0.1.0",
    },
    "basePath": "/",
    "schemes": ["http", "https"],
    "securityDefinitions": {
        "Bearer": {"type": "apiKey", "name": "Authorization", "in": "header"}
    },
}