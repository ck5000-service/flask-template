COUNTRIES = {
    'Asia Pacific': [
        {
            "country": "Australia",
            "code": "AUS",
            "number_code": "+61",
            "flag": "AUS.png"
        },
        {
            "country": "Bangladesh",
            "code": "BGD",
            "number_code": "+880",
            "flag": "BGD.png"
        },
        {
            "country": "China",
            "code": "CHN",
            "number_code": "+86",
            "flag": "CHN.png"
        },
        {
            "country": "Hong Kong",
            "code": "HKG",
            "number_code": "+852",
            "flag": "HKG.png"
        },
        {
            "country": "India",
            "code": "IND",
            "number_code": "+91",
            "flag": "IND.png"
        },
        {
            "country": "Indonesia",
            "code": "IDN",
            "number_code": "+62",
            "flag": "IDN.png"
        },
        {
            "country": "Japan",
            "code": "JPN",
            "number_code": "+81",
            "flag": "JPN.png"
        },
        {
            "country": "Malaysia",
            "code": "MYS",
            "number_code": "+60",
            "flag": "MYS.png"
        },
        {
            "country": "Myanmar",
            "code": "MMR",
            "number_code": "+95",
            "flag": "MMR.png"
        },
        {
            "country": "New Zealand",
            "code": "NZL",
            "number_code": "+64",
            "flag": "NZL.png"
        },
        {
            "country": "Philippines",
            "code": "PHL",
            "number_code": "+63",
            "flag": "PHL.png"
        },
        {
            "country": "Singapore",
            "code": "SGP",
            "number_code": "+65",
            "flag": "SGP.png"
        },
        {
            "country": "South Korea",
            "code": "PRK",
            "number_code": "+82",
            "flag": "PRK.png"
        },
        {
            "country": "Sri Lanka",
            "code": "LKA",
            "number_code": "+94",
            "flag": "LKA.png"
        },
        {
            "country": "Taiwan",
            "code": "TWN",
            "number_code": "+886",
            "flag": "TWN.png"
        },
        {
            "country": "Thailand",
            "code": "THA",
            "number_code": "+66",
            "flag": "THA.png"
        },
        {
            "country": "Vietnam",
            "code": "VNM",
            "number_code": "+84",
            "flag": "VNM.png"
        }
    ],
    'Europe': [
        {
            "country": "Austria",
            "code": "AUT",
            "number_code": "+43",
            "flag": "AUT.png"
        },
        {
            "country": "Belgium",
            "code": "BEL",
            "number_code": "+32",
            "flag": "BEL.png"
        },
        {
            "country": "Croatia",
            "code": "HRV",
            "number_code": "+385",
            "flag": "HRV.png"
        },
        {
            "country": "Czech Republic",
            "code": "CZE",
            "number_code": "+420",
            "flag": "CZE.png"
        },
        {
            "country": "Denmark",
            "code": "DNK",
            "number_code": "+45",
            "flag": "DNK.png"
        },
        {
            "country": "Finland",
            "code": "FIN",
            "number_code": "+358",
            "flag": "FIN.png"
        },
        {
            "country": "France",
            "code": "FRA",
            "number_code": "+33",
            "flag": "FRA.png"
        },
        {
            "country": "Germany",
            "code": "DEU",
            "number_code": "+49",
            "flag": "DEU.png"
        },
        {
            "country": "Greece",
            "code": "GRC",
            "number_code": "+30",
            "flag": "GRC.png"
        },
        {
            "country": "Hungary",
            "code": "HUN",
            "number_code": "+36",
            "flag": "HUN.png"
        },
        {
            "country": "Ireland",
            "code": "IRL",
            "number_code": "+353",
            "flag": "IRL.png"
        },
        {
            "country": "Italy",
            "code": "ITA",
            "number_code": "+39",
            "flag": "ITA.png"
        },
        {
            "country": "Netherlands",
            "code": "NLD",
            "number_code": "+31",
            "flag": "NLD.png"
        },
        {
            "country": "Norway",
            "code": "NOR",
            "number_code": "+47",
            "flag": "NOR.png"
        },
        {
            "country": "Poland",
            "code": "POL",
            "number_code": "+48",
            "flag": "POL.png"
        },
        {
            "country": "Portugal",
            "code": "PRT",
            "number_code": "+351",
            "flag": "PRT.png"
        },
        {
            "country": "Romania",
            "code": "ROU",
            "number_code": "+40",
            "flag": "ROU.png"
        },
        {
            "country": "Russia",
            "code": "RUS",
            "number_code": "+7",
            "flag": "RUS.png"
        },
        {
            "country": "Slovakia",
            "code": "SVK",
            "number_code": "+421",
            "flag": "SVK.png"
        },
        {
            "country": "Spain",
            "code": "ESP",
            "number_code": "+34",
            "flag": "ESP.png"
        },
        {
            "country": "Sweden",
            "code": "SWE",
            "number_code": "+46",
            "flag": "SWE.png"
        },
        {
            "country": "Switzerland",
            "code": "CHE",
            "number_code": "+41",
            "flag": "CHE.png"
        },
        {
            "country": "Turkey",
            "code": "TUR",
            "number_code": "+90",
            "flag": "TUR.png"
        },
        {
            "country": "UK",
            "code": "GBR",
            "number_code": "+44",
            "flag": "GBR.png"
        },
        {
            "country": "Ukraine",
            "code": "UKR",
            "number_code": "+380",
            "flag": "UKR.png"
        }
    ],
    'Latin America': [
        {
            "country": "Argentina",
            "code": "ARG",
            "number_code": "+54",
            "flag": "ARG.png"
        },
        {
            "country": "Brazil",
            "code": "BRA",
            "number_code": "+55",
            "flag": "BRA.png"
        },
        {
            "country": "Chile",
            "code": "CHL",
            "number_code": "+56",
            "flag": "CHL.png"
        },
        {
            "country": "Colombia",
            "code": "COL",
            "number_code": "+57",
            "flag": "COL.png"
        },
        {
            "country": "Costa Rica",
            "code": "CRI",
            "number_code": "+506",
            "flag": "CRI.png"
        },
        {
            "country": "Ecuador",
            "code": "ECU",
            "number_code": "+593",
            "flag": "ECU.png"
        },
        {
            "country": "Mexico",
            "code": "MEX",
            "number_code": "+52",
            "flag": "MEX.png"
        },
        {
            "country": "Peru",
            "code": "PER",
            "number_code": "+51",
            "flag": "PER.png"
        },
        {
            "country": "Puerto Rico",
            "code": "PRI",
            "number_code": "+1-939",
            "flag": "PRI.png"
        },
        {
            "country": "Venezuela",
            "code": "VEN",
            "number_code": "+58",
            "flag": "VEN.png"
        }
    ],
    'Middle East & Africa': [
        {
            "country": "Egypt",
            "code": "EGY",
            "number_code": "+20",
            "flag": "EGY.png"
        },
        {
            "country": "Iran",
            "code": "IRN",
            "number_code": "+98",
            "flag": "IRN.png"
        },
        {
            "country": "Israel",
            "code": "ISR",
            "number_code": "+972",
            "flag": "ISR.png"
        },
        {
            "country": "Morocco",
            "code": "MAR",
            "number_code": "+212",
            "flag": "MAR.png"
        },
        {
            "country": "Nigeria",
            "code": "NGA",
            "number_code": "+234",
            "flag": "NGA.png"
        },
        {
            "country": "Saudi Arabia",
            "code": "SAU",
            "number_code": "+966",
            "flag": "SAU.png"
        },
        {
            "country": "South Africa",
            "code": "ZAF",
            "number_code": "+27",
            "flag": "ZAF.png"
        },
        {
            "country": "UAE",
            "code": "ARE",
            "number_code": "+971",
            "flag": "ARE.png"
        }
    ],
    'North America': [
        {
            "country": "Canada",
            "code": "CAN",
            "number_code": "+1",
            "flag": "CAN.png"
        },
        {
            "country": "USA",
            "code": "USA",
            "number_code": "+1",
            "flag": "USA.png"
        }
    ]
}
