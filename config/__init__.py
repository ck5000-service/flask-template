import os
from datetime import timedelta

from dotenv import load_dotenv, find_dotenv

from config.countries import *
from config.swagger import *

load_dotenv(find_dotenv(), override=True)

FRONTEND_ENDPOINT = os.environ.get('FRONTEND_ENDPOINT', 'http://localhost:3000')
ENDPOINT = os.environ.get('ENDPOINT', 'http://localhost:5000')
SECRET_KEY = os.environ.get('SECRET_KEY', 'test')
APP_CONTACT = os.environ.get('APP_CONTACT', 'contact@isensegroup.com')

TIME_EXPIRY_TOKEN_NOT_REQUEST = timedelta(minutes=int(os.environ.get('TIME_EXPIRY_TOKEN_NOT_REQUEST', 60)))

MAX_CONTENT_LENGTH = 5  # 5MB
ALLOWED_EXCEL_EXTENSIONS = ['xlsm', 'xlsx', 'xls']

UPLOAD_FOLDER = 'uploads'

JWT_ACCESS_TOKEN_EXPIRES = timedelta(minutes=int(os.environ.get('JWT_ACCESS_TOKEN_EXPIRES', 15)))

S3_REGION = os.environ.get('S3_REGION')
S3_ACCESS_KEY = os.environ.get('S3_ACCESS_KEY')
S3_SECRET_KEY = os.environ.get('S3_SECRET_KEY')
STORAGE_FOLDER = os.environ.get('STORAGE_FOLDER')
S3_BUCKET_NAME = os.environ.get('S3_BUCKET_NAME')

SENTRY_DSN = os.environ.get('SENTRY_DSN', None)

TWILIO_ACCOUNT_ID = os.environ.get('TWILIO_ACCOUNT_ID')
TWILIO_AUTH_TOKEN = os.environ.get('TWILIO_AUTH_TOKEN')
TWILIO_FROM = os.environ.get('TWILIO_FROM')
TWILIO_FROM_MESSAGING_SERVICE_SID = os.environ.get('TWILIO_FROM_MESSAGING_SERVICE_SID')
