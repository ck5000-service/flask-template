from model import Company


def create_company(data):
    company = Company(**data)
    return company


def get_company_or_404(company_id):
    company = Company.query.get_or_404(str(company_id), 'Company does not exist')
    return company


def get_company_by_registration_number_or_404(number):
    company = Company.query.filter(Company.registration_number == number).first_or_404('Company not found')
    return company


def is_new_registration_number(number):
    company_existed = Company.query.filter(Company.registration_number == number).existed()
    return not company_existed
