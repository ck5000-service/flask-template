from flask import request
from flask_jwt_extended import get_jwt_identity

from model import User


def create_user(data):
    user = User(**data)
    return user


def get_user_by_email(email):
    user = User.query.filter(User.email == email.lower()).first()
    return user


def get_user_by_email_or_404(email):
    user = User.query.filter(User.email == email.lower()).first_or_404('Email not found')
    return user


def get_user(user_id):
    user = User.query.get(user_id)
    return user


def get_user_or_404(user_id):
    user = User.query.get_or_404(user_id, 'User not found')
    return user


def get_requester():
    if hasattr(request, 'user'):
        return request.user
    user_id = get_jwt_identity()
    user = get_user_or_404(user_id)
    request.user = user
    return user
