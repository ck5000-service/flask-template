import os
import smtplib
from email.header import Header
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.utils import formataddr

from celery_app import celery

CHARSET = "UTF-8"


def message_formatter(sender_name, from_email, to_email, subject, message):
    msg = MIMEMultipart('mixed')
    msg['Subject'] = subject
    msg['From'] = formataddr((str(Header(sender_name, 'utf-8')), from_email))
    msg['To'] = ', '.join(to_email) if isinstance(to_email, list) else to_email
    msg_body = MIMEMultipart('alternative')
    html_part = MIMEText(message.encode(CHARSET), 'html', CHARSET)
    msg_body.attach(html_part)
    msg.attach(msg_body)
    return msg


@celery.task(name='tasks.mail.send_mail')
def send_email(to_email, subject, message, sender_name=None):
    from_email = os.getenv("NO_REPLY_MAIL")
    try:
        s = smtplib.SMTP(os.getenv("AWS_SMTP_HOST"), 587)
        try:
            s.starttls()
            s.login(os.getenv("AWS_SMTP_USERNAME"), os.getenv("AWS_SMTP_PASSWORD"))
            if sender_name is None:
                sender_name = 'iSense'
            message = message_formatter(sender_name, from_email, to_email, subject, message)
            s.sendmail(from_email, to_email, message.as_string())
        except Exception as e:
            print(e)
        finally:
            s.quit()
    except Exception as e:
        print(e)
