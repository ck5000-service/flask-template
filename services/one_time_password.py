import uuid
from datetime import datetime, timedelta

from model import Company, OneTimePassword
from utils.exceptions import BadRequest


def create_otp_reset_password(user):
    otp = OneTimePassword(
        user_id=user.id,
        type=OneTimePassword.TYPE.RESET_PASSWORD,
        code=uuid.uuid4(),
        code_expiry_at=datetime.utcnow() + timedelta(minutes=5)
    )
    otp.save(is_commit=False)
    return otp


def verify_otp_reset_password(code):
    otp = OneTimePassword.query.filter(
        OneTimePassword.code == code,
        OneTimePassword.type == OneTimePassword.TYPE.RESET_PASSWORD,
    ).first_or_404('OTP not found')
    if otp.code_expiry_at < datetime.utcnow():
        raise BadRequest('OTP expired')
    return otp.user


def get_company_profile_or_404(company_id):
    company = Company.query.get_or_404(str(company_id), 'Company does not exist')
    return company


def get_company_by_registration_number_or_404(number):
    company = Company.query.filter(Company.registration_number == number).first_or_404('Company not found')
    return company


def is_new_registration_number(number):
    company_existed = Company.query.filter(Company.registration_number == number).existed()
    return not company_existed
