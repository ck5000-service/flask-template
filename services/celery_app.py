import os

from celery import Celery


def make_celery(app):
    celery = Celery(
        'tasks',
        broker=os.environ.get('CELERY_BROKER_URL'),
        backend=os.environ.get('CELERY_RESULT_BACKEND'),
        imports=(),
        include=[
            'tasks.mail',
            'tasks.sms',
            'tasks.flavor_upload',
            'tasks.realtime'
        ]
    )
    celery.conf.update(app.config)
    celery.conf.timezone = os.environ.get('CELERY_TIMEZONE')

    TaskBase = celery.Task

    class ContextTask(TaskBase):
        abstract = True

        def __call__(self, *args, **kwargs):
            with app.app_context():
                return TaskBase.__call__(self, *args, **kwargs)

    celery.Task = ContextTask
    return celery
