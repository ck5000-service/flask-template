from twilio.base.exceptions import TwilioRestException
from twilio.rest import Client

from celery_app import celery
from config import TWILIO_ACCOUNT_ID, TWILIO_AUTH_TOKEN, TWILIO_FROM, TWILIO_FROM_MESSAGING_SERVICE_SID


class SmsClient:
    __instance = None

    def __init__(self):
        if SmsClient.__instance is not None:
            raise Exception("This class is a singleton!")
        else:
            SmsClient.__instance = self

    @staticmethod
    def get_instance():
        if SmsClient.__instance is None:
            SmsClient.__instance = Client(
                TWILIO_ACCOUNT_ID,
                TWILIO_AUTH_TOKEN
            )
        return SmsClient.__instance


def is_valid_number(number):
    client = SmsClient.get_instance()
    try:
        client.lookups.phone_numbers(number).fetch()
        return True
    except TwilioRestException as e:
        if e.code == 20404:
            return False
        else:
            raise e


@celery.task(name='tasks.sms.send_sms')
def send_sms(to_phone_number, message):
    try:
        client = SmsClient.get_instance()
        client.messages.create(
            body=message,
            to=to_phone_number,
            messaging_service_sid=TWILIO_FROM_MESSAGING_SERVICE_SID,
            from_=TWILIO_FROM
        )
    except Exception as e:
        print(e)
