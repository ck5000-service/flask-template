from flasgger import swag_from
from flask import Blueprint, request
from flask_jwt_extended import jwt_required, get_jwt_identity

from model import User
from services.user import create_user, get_user_or_404
from utils.permission import authorized, ACCOUNT_PERMISSION
from utils.requester import get_pagination_params, get_sort_params
from utils.responser import generate_success_response
from utils.schema_validator import validated
from .validation import *

bp = Blueprint('user', __name__, url_prefix='/users')

map_sort = {
    'name': User.name,
    'status': User.status
}


@bp.route('', methods=['GET'])
@swag_from('./docs/get_list.yml')
def get_list():
    sort = get_sort_params(request, map_sort, User.created_at)
    pagination = get_pagination_params(request)
    status = request.args.get("status", None)

    query = User.query
    if status is not None:
        query = query.filter(User.status == status)
    companies, count = query.sort(**sort).get_with_paginate(**pagination)

    data = [c.to_json() for c in companies]
    return generate_success_response(data=data, **pagination, total=count)


@bp.route('', methods=['POST'])
@jwt_required
@authorized(roles=ACCOUNT_PERMISSION.values())
@validated(create_schema)
@swag_from('./docs/create.yml')
def create():
    body = request.data
    user = create_user(body)
    user.save()
    return generate_success_response(user.to_json())


@bp.route('/<uuid:user_id>', methods=['GET'])
@jwt_required
@authorized(roles=ACCOUNT_PERMISSION.values())
@swag_from('./docs/get_one.yml')
def get_one(user_id):
    user = get_user_or_404(user_id)
    data = user.to_json()
    return generate_success_response(data)


@bp.route('/me', methods=['GET'])
@jwt_required
@authorized(roles=ACCOUNT_PERMISSION.values())
@swag_from('./docs/get_me.yml')
def get_me():
    user_id = get_jwt_identity()
    user = get_user_or_404(user_id)
    data = user.to_json()
    return generate_success_response(data)


@bp.route('/<uuid:user_id>', methods=['PUT'])
@jwt_required
@authorized(roles=ACCOUNT_PERMISSION.values())
@validated(update_schema)
@swag_from('./docs/update.yml')
def update(user_id):
    body = request.data
    user = get_user_or_404(user_id)
    user.update(body)
    return generate_success_response(user.to_json())


@bp.route('/me', methods=['PUT'])
@jwt_required
@authorized(roles=ACCOUNT_PERMISSION.values())
@validated(update_schema)
@swag_from('./docs/update.yml')
def update_me():
    user_id = get_jwt_identity()
    body = request.data
    user = get_user_or_404(user_id)
    user.update(body)
    return generate_success_response(user.to_json())


@bp.route('/<uuid:user_id>', methods=['DELETE'])
@jwt_required
@authorized(roles=ACCOUNT_PERMISSION.values())
@swag_from('./docs/delete.yml')
def delete(user_id):
    user = get_user_or_404(user_id)
    user.delete()
    return generate_success_response()
