from voluptuous import Optional, Schema, All, Required, Length

from utils.schema_validator import email_schema, number_schema, string_schema, country_code_schema, \
    string_or_none_schema, country_number_code_schema, uuid_schema

create_schema = Schema({
    Required('email'): email_schema,
    Required('title'): string_schema,
    Required('name'): string_schema,
    Required('family_name'): string_schema,

    Required('phone_number'): number_schema,
    Required('country_code_phone_number'): country_code_schema,
    Required('country_phone_number'): country_number_code_schema,

    Required('office_number'): number_schema,
    Required('country_code_office_number'): country_code_schema,
    Required('country_office_number'): country_number_code_schema,
})

update_schema = Schema({
    Optional('email'): email_schema,
    Optional('title'): string_schema,
    Optional('name'): string_schema,
    Optional('family_name'): string_schema,

    Optional('phone_number'): number_schema,
    Optional('country_code_phone_number'): country_code_schema,
    Optional('country_phone_number'): country_number_code_schema,

    Optional('office_number'): number_schema,
    Optional('country_code_office_number'): country_code_schema,
    Optional('country_office_number'): country_number_code_schema,
})

update_status_schema = Schema({
    Required('user_ids'): All([uuid_schema], Length(min=1, max=10)),
    Optional('reason'): string_or_none_schema
})
