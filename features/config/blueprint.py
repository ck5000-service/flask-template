import os
from itertools import chain

from flasgger import swag_from
from flask import Blueprint, current_app

from config import COUNTRIES
from utils.responser import generate_success_response

bp = Blueprint('config', __name__, url_prefix='/conf')


@bp.route('/countries', methods=['GET'])
@swag_from('./docs/get_list_countries.yml')
def get_list_countries():
    data = list(chain(*list(COUNTRIES.values())))
    return generate_success_response(data=data)


@bp.route('/flags/<flag>', methods=['GET'])
@swag_from('./docs/get_flag.yml')
def get_flag(flag):
    path = flag
    path = os.path.join('flags', path)
    return current_app.send_static_file(path)


@bp.route('/region', methods=['GET'])
@swag_from('./docs/get_list_region.yml')
def get_list_region():
    data = COUNTRIES
    return generate_success_response(data=data)


@bp.route('/static/<path:path>', methods=['GET'])
@swag_from('./docs/static.yml')
def send_static_file(path):
    return current_app.send_static_file(path)
