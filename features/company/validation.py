from voluptuous import Schema, Optional, Required

from utils.schema_validator import country_schema, string_schema

create_schema = Schema({
    Required('name'): string_schema,
    Required('registration_number'): string_schema,
    Optional('website'): string_schema,
    Optional('address1'): string_schema,
    Optional('address2'): string_schema,
    Optional('state'): string_schema,
    Optional('city'): string_schema,
    Optional('zip_code'): string_schema,
    Optional('country'): country_schema,
})

update_schema = Schema({
    Optional('name'): string_schema,
    Optional('registration_number'): string_schema,
    Optional('website'): string_schema,
    Optional('address1'): string_schema,
    Optional('address2'): string_schema,
    Optional('state'): string_schema,
    Optional('city'): string_schema,
    Optional('zip_code'): string_schema,
    Optional('country'): country_schema,
})
