from flasgger import swag_from
from flask import Blueprint, request
from flask_jwt_extended import jwt_required

from model import Company
from services.company import create_company, get_company_or_404, is_new_registration_number
from utils.exceptions import BadRequest
from utils.permission import authorized, ACCOUNT_PERMISSION
from utils.requester import get_pagination_params, get_sort_params
from utils.responser import generate_success_response
from utils.schema_validator import validated
from .validation import *

bp = Blueprint('company', __name__, url_prefix='/companies')

map_sort = {
    'name': Company.name,
    'country': Company.country,
    'type': Company.type,
}


@bp.route('', methods=['GET'])
@swag_from('./docs/get_list.yml')
def get_list():
    sort = get_sort_params(request, map_sort, Company.created_at)
    pagination = get_pagination_params(request)
    r_type = request.args.get("type", None)
    status = request.args.get("status", None)

    query = Company.query
    if r_type is not None:
        query = query.filter(Company.type == r_type)
    if status is not None:
        query = query.filter(Company.status == status)
    companies, count = query.sort(**sort).get_with_paginate(**pagination)

    data = [c.to_json() for c in companies]
    return generate_success_response(data=data, **pagination, total=count)


@bp.route('', methods=['POST'])
@jwt_required
@authorized(roles=[ACCOUNT_PERMISSION.ADMIN])
@validated(create_schema)
@swag_from('./docs/create.yml')
def create():
    body = request.data
    company = create_company(body)
    company.save()
    return generate_success_response(company.to_json())


@bp.route('/<uuid:company_id>', methods=['GET'])
@swag_from('./docs/get_one.yml')
def get_one(company_id):
    company = get_company_or_404(company_id)
    data = company.to_json()
    return generate_success_response(data)


@bp.route('/<uuid:company_id>', methods=['PUT'])
@jwt_required
@authorized(roles=[ACCOUNT_PERMISSION.ADMIN])
@validated(update_schema)
@swag_from('./docs/update.yml')
def update(company_id):
    body = request.data
    company = get_company_or_404(company_id)
    if 'registration_number' in body and not is_new_registration_number(body['registration_number']):
        raise BadRequest(f'registration number is existed: {body["registration_number"]}')
    company.update(body)
    return generate_success_response(company.to_json())


@bp.route('/<uuid:company_id>', methods=['DELETE'])
@jwt_required
@authorized(roles=[ACCOUNT_PERMISSION.ADMIN])
@swag_from('./docs/delete.yml')
def delete(company_id):
    company = get_company_or_404(company_id)
    company.delete()
    return generate_success_response()
