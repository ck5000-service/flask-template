from datetime import timedelta

from flasgger import swag_from
from flask import Blueprint, request
from flask_jwt_extended import (jwt_required,
                                jwt_refresh_token_required,
                                get_raw_jwt,
                                create_access_token, create_refresh_token, get_jwt_identity)

from model import RevokedToken
from model.db import db
from services.one_time_password import create_otp_reset_password, verify_otp_reset_password
from services.user import create_user, get_user_by_email, get_requester, get_user_by_email_or_404
from tasks.mail import send_mail_reset_password, send_mail_welcome
from utils.responser import generate_success_response
from utils.schema_validator import validated
from .validation import *

bp = Blueprint('auth', __name__, url_prefix='/auth')


@bp.route('/register', methods=['POST'])
@validated(register_schema)
@swag_from('./docs/register.yml')
def register():
    data = request.data
    if get_user_by_email(data['email']) is not None:
        raise BadRequest('email already registered')
    password = data.pop('password')
    user = create_user(data)
    user.set_password(password)
    user.save()
    send_mail_welcome(user)
    return generate_success_response(user.to_json())


@bp.route('/login', methods=['POST'])
@validated(login_schema)
@swag_from('./docs/login.yml')
def login():
    data = request.data
    user = get_user_by_email(data['email'])
    if not user:
        raise BadRequest('User not exist')
    if not user.check_password(data['password']):
        raise BadRequest('Wrong password')

    access_token = create_access_token(identity=str(user.id), expires_delta=timedelta(hours=12))
    refresh_token = create_refresh_token(identity=str(user.id))
    data = {
        'access_token': access_token,
        'refresh_token': refresh_token,
        'user': user.to_json()
    }
    db.session.commit()
    return generate_success_response(data)


@bp.route('/logout-access', methods=['POST'])
@jwt_required
@swag_from('./docs/logout_access.yml')
def logout_access():
    jti = get_raw_jwt()['jti']
    revoked_token = RevokedToken(jti=jti)
    revoked_token.add()
    return generate_success_response()


@bp.route('/logout-refresh', methods=['POST'])
@jwt_refresh_token_required
@swag_from('./docs/logout_refresh.yml')
def logout_refresh():
    jti = get_raw_jwt()['jti']
    revoked_token = RevokedToken(jti=jti)
    revoked_token.add()
    return generate_success_response()


@bp.route('/token-refresh', methods=['POST'])
@jwt_refresh_token_required
@swag_from('./docs/token_refresh.yml')
def token_refresh():
    user_id = get_jwt_identity()
    access_token = create_access_token(identity=user_id)
    data = {'access_token': access_token}
    return generate_success_response(data)


@bp.route('/change-password', methods=['POST'])
@jwt_required
@validated(change_password_schema)
@swag_from('./docs/change_password.yml')
def change_password():
    user = get_requester()
    body = request.data
    if not user.check_password(body['old_password']):
        raise BadRequest('Password is incorrect')
    user.set_password(body['password'])
    db.session.commit()
    return generate_success_response()


@bp.route('/forgot-password', methods=['POST'])
@validated(forgot_password_schema)
@swag_from('./docs/forgot_password.yml')
def forgot_password():
    body = request.data
    user = get_user_by_email_or_404(body['email'])
    otp = create_otp_reset_password(user)
    otp.save()
    send_mail_reset_password(user, otp.code)
    return generate_success_response()


@bp.route('/reset-password', methods=['POST'])
@validated(reset_password_schema)
@swag_from('./docs/reset_password.yml')
def reset_password():
    body = request.data
    user = verify_otp_reset_password(body['reset_password_code'])
    user.set_password(body['password'])
    db.session.commit()
    return generate_success_response(user.to_json())
