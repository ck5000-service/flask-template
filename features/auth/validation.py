from voluptuous import Schema, Required, All

from utils.exceptions import BadRequest
from utils.schema_validator import (email_schema,
                                    number_schema,
                                    password_schema,
                                    string_schema,
                                    country_code_schema,
                                    country_number_code_schema,
                                    uuid_schema)


def password_confirm_must_match(payload):
    password_confirm = payload.pop('password_confirm')
    if payload['password'] != password_confirm:
        raise BadRequest('password confirm must match')
    return payload


register_schema = Schema(All({
    Required('company_id'): uuid_schema,

    Required('email'): email_schema,
    Required('title'): string_schema,
    Required('name'): string_schema,
    Required('family_name'): string_schema,

    Required('phone_number'): number_schema,
    Required('country_code_phone_number'): country_code_schema,
    Required('country_phone_number'): country_number_code_schema,

    Required('office_number'): number_schema,
    Required('country_code_office_number'): country_code_schema,
    Required('country_office_number'): country_number_code_schema,

    Required('password'): password_schema,
    Required('password_confirm'): string_schema,
}, password_confirm_must_match))

login_schema = Schema({
    Required('email'): email_schema,
    Required('password'): string_schema
})

change_password_schema = Schema(All({
    Required('old_password'): string_schema,
    Required('password'): password_schema,
    Required('password_confirm'): string_schema
}, password_confirm_must_match))

forgot_password_schema = Schema({
    Required('email'): email_schema
})

reset_password_schema = Schema(All({
    Required('password'): password_schema,
    Required('password_confirm'): string_schema,
    Required('reset_password_code'): string_schema
}, password_confirm_must_match))
