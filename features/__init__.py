from .config import bp as config_bp
from .user import bp as user_bp
from .auth import bp as auth_bp
from .company import bp as company_bp


def register_blueprint(app):
    app.register_blueprint(config_bp)
    app.register_blueprint(user_bp)
    app.register_blueprint(auth_bp)
    app.register_blueprint(company_bp)

